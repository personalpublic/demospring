package com.josheluis.demo.entity;

import com.fasterxml.jackson.annotation.*;
import java.util.List;

public class Result {
    private Meta meta;
    private List<Long> timestamp;
    private Indicators indicators;

    @JsonProperty("meta")
    public Meta getMeta() { return meta; }
    @JsonProperty("meta")
    public void setMeta(Meta value) { this.meta = value; }

    @JsonProperty("timestamp")
    public List<Long> getTimestamp() { return timestamp; }
    @JsonProperty("timestamp")
    public void setTimestamp(List<Long> value) { this.timestamp = value; }

    @JsonProperty("indicators")
    public Indicators getIndicators() { return indicators; }
    @JsonProperty("indicators")
    public void setIndicators(Indicators value) { this.indicators = value; }
}
