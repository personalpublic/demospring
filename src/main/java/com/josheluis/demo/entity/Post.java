package com.josheluis.demo.entity;

import com.fasterxml.jackson.annotation.*;

public class Post {
    private String timezone;
    private Long start;
    private Long end;
    private Long gmtoffset;

    @JsonProperty("timezone")
    public String getTimezone() { return timezone; }
    @JsonProperty("timezone")
    public void setTimezone(String value) { this.timezone = value; }

    @JsonProperty("start")
    public Long getStart() { return start; }
    @JsonProperty("start")
    public void setStart(Long value) { this.start = value; }

    @JsonProperty("end")
    public Long getEnd() { return end; }
    @JsonProperty("end")
    public void setEnd(Long value) { this.end = value; }

    @JsonProperty("gmtoffset")
    public Long getGmtoffset() { return gmtoffset; }
    @JsonProperty("gmtoffset")
    public void setGmtoffset(Long value) { this.gmtoffset = value; }
}
