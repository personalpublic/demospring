package com.josheluis.demo.entity;

import com.fasterxml.jackson.annotation.*;
import java.util.List;

public class Indicators {
    private List<Quote> quote;

    @JsonProperty("quote")
    public List<Quote> getQuote() { return quote; }
    @JsonProperty("quote")
    public void setQuote(List<Quote> value) { this.quote = value; }
}
