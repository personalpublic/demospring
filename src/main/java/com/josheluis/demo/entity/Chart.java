package com.josheluis.demo.entity;

import com.fasterxml.jackson.annotation.*;
import java.util.List;

public class Chart {
    private List<Result> result;
    private Object error;

    @JsonProperty("result")
    public List<Result> getResult() { return result; }
    @JsonProperty("result")
    public void setResult(List<Result> value) { this.result = value; }

    @JsonProperty("error")
    public Object getError() { return error; }
    @JsonProperty("error")
    public void setError(Object value) { this.error = value; }
}
