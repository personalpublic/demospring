package com.josheluis.demo.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Request {
	@NotNull
	String desde;
	@NotNull
	String hasta;
	Date desdeDate;
	Date hastaDate;
}
