package com.josheluis.demo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.*;

@Document(collection = "RequestYahoo")
public class ParametrosObject {
	@Id
	String id;

	private Chart chart;

	@JsonProperty("chart")
	public Chart getChart() {
		return chart;
	}

	@JsonProperty("chart")
	public void setChart(Chart value) {
		this.chart = value;
	}
}
