package com.josheluis.demo.entity;

import com.fasterxml.jackson.annotation.*;
import java.util.List;

public class Meta {
    private String currency;
    private String symbol;
    private String exchangeName;
    private String instrumentType;
    private Long firstTradeDate;
    private Long regularMarketTime;
    private Long gmtoffset;
    private String timezone;
    private String exchangeTimezoneName;
    private Double regularMarketPrice;
    private Double chartPreviousClose;
    private Double previousClose;
    private Long scale;
    private Long priceHint;
    private CurrentTradingPeriod currentTradingPeriod;
    private List<List<Post>> tradingPeriods;
    private String dataGranularity;
    private String range;
    private List<String> validRanges;

    @JsonProperty("currency")
    public String getCurrency() { return currency; }
    @JsonProperty("currency")
    public void setCurrency(String value) { this.currency = value; }

    @JsonProperty("symbol")
    public String getSymbol() { return symbol; }
    @JsonProperty("symbol")
    public void setSymbol(String value) { this.symbol = value; }

    @JsonProperty("exchangeName")
    public String getExchangeName() { return exchangeName; }
    @JsonProperty("exchangeName")
    public void setExchangeName(String value) { this.exchangeName = value; }

    @JsonProperty("instrumentType")
    public String getInstrumentType() { return instrumentType; }
    @JsonProperty("instrumentType")
    public void setInstrumentType(String value) { this.instrumentType = value; }

    @JsonProperty("firstTradeDate")
    public Long getFirstTradeDate() { return firstTradeDate; }
    @JsonProperty("firstTradeDate")
    public void setFirstTradeDate(Long value) { this.firstTradeDate = value; }

    @JsonProperty("regularMarketTime")
    public Long getRegularMarketTime() { return regularMarketTime; }
    @JsonProperty("regularMarketTime")
    public void setRegularMarketTime(Long value) { this.regularMarketTime = value; }

    @JsonProperty("gmtoffset")
    public Long getGmtoffset() { return gmtoffset; }
    @JsonProperty("gmtoffset")
    public void setGmtoffset(Long value) { this.gmtoffset = value; }

    @JsonProperty("timezone")
    public String getTimezone() { return timezone; }
    @JsonProperty("timezone")
    public void setTimezone(String value) { this.timezone = value; }

    @JsonProperty("exchangeTimezoneName")
    public String getExchangeTimezoneName() { return exchangeTimezoneName; }
    @JsonProperty("exchangeTimezoneName")
    public void setExchangeTimezoneName(String value) { this.exchangeTimezoneName = value; }

    @JsonProperty("regularMarketPrice")
    public Double getRegularMarketPrice() { return regularMarketPrice; }
    @JsonProperty("regularMarketPrice")
    public void setRegularMarketPrice(Double value) { this.regularMarketPrice = value; }

    @JsonProperty("chartPreviousClose")
    public Double getChartPreviousClose() { return chartPreviousClose; }
    @JsonProperty("chartPreviousClose")
    public void setChartPreviousClose(Double value) { this.chartPreviousClose = value; }

    @JsonProperty("previousClose")
    public Double getPreviousClose() { return previousClose; }
    @JsonProperty("previousClose")
    public void setPreviousClose(Double value) { this.previousClose = value; }

    @JsonProperty("scale")
    public Long getScale() { return scale; }
    @JsonProperty("scale")
    public void setScale(Long value) { this.scale = value; }

    @JsonProperty("priceHint")
    public Long getPriceHint() { return priceHint; }
    @JsonProperty("priceHint")
    public void setPriceHint(Long value) { this.priceHint = value; }

    @JsonProperty("currentTradingPeriod")
    public CurrentTradingPeriod getCurrentTradingPeriod() { return currentTradingPeriod; }
    @JsonProperty("currentTradingPeriod")
    public void setCurrentTradingPeriod(CurrentTradingPeriod value) { this.currentTradingPeriod = value; }

    @JsonProperty("tradingPeriods")
    public List<List<Post>> getTradingPeriods() { return tradingPeriods; }
    @JsonProperty("tradingPeriods")
    public void setTradingPeriods(List<List<Post>> value) { this.tradingPeriods = value; }

    @JsonProperty("dataGranularity")
    public String getDataGranularity() { return dataGranularity; }
    @JsonProperty("dataGranularity")
    public void setDataGranularity(String value) { this.dataGranularity = value; }

    @JsonProperty("range")
    public String getRange() { return range; }
    @JsonProperty("range")
    public void setRange(String value) { this.range = value; }

    @JsonProperty("validRanges")
    public List<String> getValidRanges() { return validRanges; }
    @JsonProperty("validRanges")
    public void setValidRanges(List<String> value) { this.validRanges = value; }
}
