package com.josheluis.demo.entity;

import java.util.ArrayList;
import java.util.List;

import com.josheluis.demo.entitydweet.DweetRequest;

import lombok.Data;

@Data
public class ResposeObject {
	String code;
	String mensaje;
	List<YahooValue> list = new ArrayList<YahooValue>();
	List<DweetRequest> list2 = new ArrayList<DweetRequest>();
}
