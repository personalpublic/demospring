package com.josheluis.demo.entity;

import com.fasterxml.jackson.annotation.*;

public class CurrentTradingPeriod {
    private Post pre;
    private Post regular;
    private Post post;

    @JsonProperty("pre")
    public Post getPre() { return pre; }
    @JsonProperty("pre")
    public void setPre(Post value) { this.pre = value; }

    @JsonProperty("regular")
    public Post getRegular() { return regular; }
    @JsonProperty("regular")
    public void setRegular(Post value) { this.regular = value; }

    @JsonProperty("post")
    public Post getPost() { return post; }
    @JsonProperty("post")
    public void setPost(Post value) { this.post = value; }
}
