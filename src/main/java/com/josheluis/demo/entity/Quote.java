package com.josheluis.demo.entity;

import com.fasterxml.jackson.annotation.*;
import java.util.List;

public class Quote {
    private List<Double> close;
    private List<Long> volume;
    private List<Double> high;
    private List<Double> low;
    private List<Double> open;

    @JsonProperty("close")
    public List<Double> getClose() { return close; }
    @JsonProperty("close")
    public void setClose(List<Double> value) { this.close = value; }

    @JsonProperty("volume")
    public List<Long> getVolume() { return volume; }
    @JsonProperty("volume")
    public void setVolume(List<Long> value) { this.volume = value; }

    @JsonProperty("high")
    public List<Double> getHigh() { return high; }
    @JsonProperty("high")
    public void setHigh(List<Double> value) { this.high = value; }

    @JsonProperty("low")
    public List<Double> getLow() { return low; }
    @JsonProperty("low")
    public void setLow(List<Double> value) { this.low = value; }

    @JsonProperty("open")
    public List<Double> getOpen() { return open; }
    @JsonProperty("open")
    public void setOpen(List<Double> value) { this.open = value; }
}
