package com.josheluis.demo.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "YahooValue")
public class YahooValue {
	@Id
	private Long timestap;
	Date timeDate;
    private Double close;
    private Long volume;
    private Double high;
    private Double low;
    private Double open;
}
