package com.josheluis.demo.rest;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class DweetGet {

	public String getFinancingFivedays() {
		try {
			RestTemplate restTemplate = new RestTemplate();
			String fooResourceUrl  = "https://dweet.io/get/latest/dweet/for/thecore?callback=dweetCallback.callback0&_=1599346581949";
			return restTemplate.getForObject(fooResourceUrl, String.class);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}

	}
	

}
