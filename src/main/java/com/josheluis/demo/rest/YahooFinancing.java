package com.josheluis.demo.rest;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.josheluis.demo.entity.Converter;
import com.josheluis.demo.entity.ParametrosObject;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class YahooFinancing {

	public ParametrosObject getFinancingFivedays() {
		try {
			ParametrosObject object = new ParametrosObject();
			RestTemplate restTemplate = new RestTemplate();
			String fooResourceUrl  = "https://query1.finance.yahoo.com/v8/finance/chart/EURUSD=X?region=ES&lang=es-ES&includePrePost=false&interval=5m&range=5d&corsDomain=es.finance.yahoo.com&.tsrc=finance";
			String response  = restTemplate.getForObject(fooResourceUrl, String.class);
			object = Converter.fromJsonString(response);
			return object;
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}

	}
}
