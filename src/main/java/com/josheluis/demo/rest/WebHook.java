package com.josheluis.demo.rest;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.josheluis.demo.entity.ResposeObject;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class WebHook {

	public boolean webhook(ResposeObject object) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.postForObject("https://webhook.site/fd8e357d-2903-4092-b145-720cf7778ad2", object, String.class);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
			return false;
		}
	}
}
