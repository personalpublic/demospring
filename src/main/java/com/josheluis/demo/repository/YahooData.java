package com.josheluis.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.josheluis.demo.entity.YahooValue;

public interface YahooData extends MongoRepository<YahooValue, String> {
	
	public List<YahooValue> findByTimeDateBetween(Date desde,Date hasta);
	
}
