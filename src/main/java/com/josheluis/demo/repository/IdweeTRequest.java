package com.josheluis.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.josheluis.demo.entitydweet.DweetRequest;

@Repository
public interface IdweeTRequest extends MongoRepository<DweetRequest, String> {

}
