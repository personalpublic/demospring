package com.josheluis.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.josheluis.demo.entity.ParametrosObject;

@Repository
public interface Yahoorepository extends MongoRepository<ParametrosObject, String> {

}
