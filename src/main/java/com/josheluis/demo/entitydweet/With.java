package com.josheluis.demo.entitydweet;

import com.fasterxml.jackson.annotation.*;

public class With {
    private String thing;
    private String created;
    private Content content;

    @JsonProperty("thing")
    public String getThing() { return thing; }
    @JsonProperty("thing")
    public void setThing(String value) { this.thing = value; }

    @JsonProperty("created")
    public String getCreated() { return created; }
    @JsonProperty("created")
    public void setCreated(String value) { this.created = value; }

    @JsonProperty("content")
    public Content getContent() { return content; }
    @JsonProperty("content")
    public void setContent(Content value) { this.content = value; }
}
