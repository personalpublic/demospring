package com.josheluis.demo.entitydweet;

import com.fasterxml.jackson.annotation.*;

public class Content {
    private Double temperature;
    private Long humidity;
    private Double temperature2;
    private Long humidity2;
    private Double temperature3;
    private Long humidity3;
    private String temperature4;
    private Long humidity4;
    private Long temperature5;
    private Long temperature6;
    private Double vwc;
    private Long lux;
    private Long tensio;
    private Long r1;
    private Long r2;
    private Long r3;
    private Long r4;
    private Long r5;
    private Long r6;
    private Long r7;
    private Long l8;

    @JsonProperty("temperature")
    public Double getTemperature() { return temperature; }
    @JsonProperty("temperature")
    public void setTemperature(Double value) { this.temperature = value; }

    @JsonProperty("humidity")
    public Long getHumidity() { return humidity; }
    @JsonProperty("humidity")
    public void setHumidity(Long value) { this.humidity = value; }

    @JsonProperty("temperature2")
    public Double getTemperature2() { return temperature2; }
    @JsonProperty("temperature2")
    public void setTemperature2(Double value) { this.temperature2 = value; }

    @JsonProperty("humidity2")
    public Long getHumidity2() { return humidity2; }
    @JsonProperty("humidity2")
    public void setHumidity2(Long value) { this.humidity2 = value; }

    @JsonProperty("temperature3")
    public Double getTemperature3() { return temperature3; }
    @JsonProperty("temperature3")
    public void setTemperature3(Double value) { this.temperature3 = value; }

    @JsonProperty("humidity3")
    public Long getHumidity3() { return humidity3; }
    @JsonProperty("humidity3")
    public void setHumidity3(Long value) { this.humidity3 = value; }

    @JsonProperty("temperature4")
    public String getTemperature4() { return temperature4; }
    @JsonProperty("temperature4")
    public void setTemperature4(String value) { this.temperature4 = value; }

    @JsonProperty("humidity4")
    public Long getHumidity4() { return humidity4; }
    @JsonProperty("humidity4")
    public void setHumidity4(Long value) { this.humidity4 = value; }

    @JsonProperty("temperature5")
    public Long getTemperature5() { return temperature5; }
    @JsonProperty("temperature5")
    public void setTemperature5(Long value) { this.temperature5 = value; }

    @JsonProperty("temperature6")
    public Long getTemperature6() { return temperature6; }
    @JsonProperty("temperature6")
    public void setTemperature6(Long value) { this.temperature6 = value; }

    @JsonProperty("VWC")
    public Double getVwc() { return vwc; }
    @JsonProperty("VWC")
    public void setVwc(Double value) { this.vwc = value; }

    @JsonProperty("lux")
    public Long getLux() { return lux; }
    @JsonProperty("lux")
    public void setLux(Long value) { this.lux = value; }

    @JsonProperty("tensio")
    public Long getTensio() { return tensio; }
    @JsonProperty("tensio")
    public void setTensio(Long value) { this.tensio = value; }

    @JsonProperty("R1")
    public Long getR1() { return r1; }
    @JsonProperty("R1")
    public void setR1(Long value) { this.r1 = value; }

    @JsonProperty("R2")
    public Long getR2() { return r2; }
    @JsonProperty("R2")
    public void setR2(Long value) { this.r2 = value; }

    @JsonProperty("R3")
    public Long getR3() { return r3; }
    @JsonProperty("R3")
    public void setR3(Long value) { this.r3 = value; }

    @JsonProperty("R4")
    public Long getR4() { return r4; }
    @JsonProperty("R4")
    public void setR4(Long value) { this.r4 = value; }

    @JsonProperty("R5")
    public Long getR5() { return r5; }
    @JsonProperty("R5")
    public void setR5(Long value) { this.r5 = value; }

    @JsonProperty("R6")
    public Long getR6() { return r6; }
    @JsonProperty("R6")
    public void setR6(Long value) { this.r6 = value; }

    @JsonProperty("R7")
    public Long getR7() { return r7; }
    @JsonProperty("R7")
    public void setR7(Long value) { this.r7 = value; }

    @JsonProperty("L8")
    public Long getL8() { return l8; }
    @JsonProperty("L8")
    public void setL8(Long value) { this.l8 = value; }
}
