package com.josheluis.demo.entitydweet;

import com.fasterxml.jackson.annotation.*;

import lombok.Data;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "DweetRequest")
public class DweetRequest {
	@Id
	String id;
    private String dweetRequestThis;
    private String by;
    private String the;
    private List<With> with;

    @JsonProperty("this")
    public String getDweetRequestThis() { return dweetRequestThis; }
    @JsonProperty("this")
    public void setDweetRequestThis(String value) { this.dweetRequestThis = value; }

    @JsonProperty("by")
    public String getBy() { return by; }
    @JsonProperty("by")
    public void setBy(String value) { this.by = value; }

    @JsonProperty("the")
    public String getThe() { return the; }
    @JsonProperty("the")
    public void setThe(String value) { this.the = value; }

    @JsonProperty("with")
    public List<With> getWith() { return with; }
    @JsonProperty("with")
    public void setWith(List<With> value) { this.with = value; }
}
