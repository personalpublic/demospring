package com.josheluis.demo.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import com.josheluis.demo.entity.Request;
import com.josheluis.demo.entity.ResposeObject;
import com.josheluis.demo.entity.YahooValue;
import com.josheluis.demo.repository.YahooData;
import com.josheluis.demo.rest.WebHook;
import com.josheluis.demo.service.LoadData;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping(value = "/joshe")
public class Endpoint {

	@Autowired
	LoadData data;
	
	@Autowired
	YahooData yahoodate;
	
	@Autowired
	WebHook webhook;

	@GetMapping(value = "/yahooInsert")
	public ResposeObject yahooValor() {
		
		return data.loadfinancingData();
	}
	@GetMapping(value = "/yahooTime")
	public ResposeObject dweetRequest(){
		return data.dweetRequest();
	}
	@PostMapping(value = "/yahooSelect")
	public ResposeObject yahooValor(@Valid @RequestBody Request request) {
		SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		ResposeObject object = new ResposeObject();
		object.setList(new ArrayList<YahooValue>());
		try {
		request.setDesdeDate(DateFor.parse(request.getDesde()));
		request.setHastaDate(DateFor.parse( request.getHasta()));
		object.setCode("0");
		object.setMensaje("Successful Process");
		object.getList().addAll(yahoodate.findByTimeDateBetween(request.getDesdeDate(), request.getHastaDate())) ;
		webhook.webhook(object);
		return object;
		} catch (Exception e) {
			object.setCode("-1");
			object.setMensaje("failed process: "+ e.getMessage());
			log.info(e.getMessage());
			return object;
		}	
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResposeObject handleException(MethodArgumentNotValidException exception) {
		ResposeObject response = new ResposeObject();
		StringBuilder bld = new StringBuilder();
		for (FieldError var : exception.getBindingResult().getFieldErrors()) {
			bld.append("La columna " + var.getField() + " " + var.getDefaultMessage() + ", ");
		}
		log.error(bld.toString());
		response.setMensaje(bld.toString());
		response.setCode("-1");
		return response;
	}
}
