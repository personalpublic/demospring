package com.josheluis.demo.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.josheluis.demo.entity.ParametrosObject;
import com.josheluis.demo.entity.ResposeObject;
import com.josheluis.demo.entity.YahooValue;
import com.josheluis.demo.entitydweet.Converter;
import com.josheluis.demo.entitydweet.DweetRequest;
import com.josheluis.demo.repository.IdweeTRequest;
import com.josheluis.demo.repository.YahooData;
import com.josheluis.demo.repository.Yahoorepository;
import com.josheluis.demo.rest.DweetGet;
import com.josheluis.demo.rest.WebHook;
import com.josheluis.demo.rest.YahooFinancing;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class LoadData {
	
	@Autowired
	Yahoorepository iyahoorepository;
	
	@Autowired
	YahooFinancing financing;
	
	@Autowired
	YahooData data;
	
	@Autowired
	WebHook webhook;
	
	@Autowired
	DweetGet dweetGet;
	
	@Autowired
	IdweeTRequest idweeTRequest;
	
	public ResposeObject loadfinancingData() {
		ResposeObject object = new ResposeObject();
		try {
			ParametrosObject param = financing.getFinancingFivedays();
			List<YahooValue> values = new ArrayList<>();
	        for(int i =0; i<param.getChart().getResult().get(0).getTimestamp().size(); i++){
	           YahooValue value = new YahooValue();
	           value.setTimestap(param.getChart().getResult().get(0).getTimestamp().get(i));
               Timestamp ts=new Timestamp(value.getTimestap()*1000);  
               Date date=new Date(ts.getTime());  
	           value.setTimeDate(date);
	           value.setVolume(param.getChart().getResult().get(0).getIndicators().getQuote().get(0).getVolume().get(i));
	           value.setClose(param.getChart().getResult().get(0).getIndicators().getQuote().get(0).getClose().get(i));
	           value.setHigh(param.getChart().getResult().get(0).getIndicators().getQuote().get(0).getHigh().get(i));
	           value.setLow(param.getChart().getResult().get(0).getIndicators().getQuote().get(0).getLow().get(i));
	           value.setOpen(param.getChart().getResult().get(0).getIndicators().getQuote().get(0).getOpen().get(i));
	           values.add(value);
	        }
			iyahoorepository.save(param);
	        data.saveAll(values);
			object.setCode("0");
			object.setMensaje("Successful Process, insert data: "+values.stream().count());
			return object;
		} catch (Exception e) {
			log.error(e.getMessage());
			object.setCode("-1");
			object.setMensaje("failed process: "+ e.getMessage());
			return object;
		}
	}
	
	public ResposeObject dweetRequest(){
		ResposeObject object= new ResposeObject();
		List<DweetRequest> dweetRequests = new ArrayList<DweetRequest>();
		List<String> dweetRequestString = new ArrayList<String>();

		try {
			IntStream.range(0,15).forEach(idx->{
				dweetRequestString.add(dweetGet.getFinancingFivedays());
				try {
					TimeUnit.MINUTES.sleep(1);
				} catch (Exception e) {
					log.error(e.getMessage());
				}
				
			});
			dweetRequestString.parallelStream().forEach(strin -> {
				try {
					dweetRequests.add(Converter.fromJsonString(extracRegex(strin,"\\{.*\\}",0)));
				} catch (Exception e2) {
					log.error(e2.getMessage());
				}
				
			});
			idweeTRequest.saveAll(dweetRequests);
			object.setCode("0");
			object.setMensaje("Successful Process, insert data: "+dweetRequests.stream().count());
			object.setList2(dweetRequests);
			webhook.webhook(object);
			return object;
		} catch (Exception e) {
			object.setCode("-1");
			object.setMensaje("failed process: "+ e.getMessage());
			return object;
		}
	}
	private String extracRegex(String txt, String regex,int group) {
		try {
			Pattern r = Pattern.compile(regex);
			Matcher m = r.matcher(txt);
		      if (m.find( )) {
		          return m.group(group);
		       } else {
		    	   log.error("NO MATCH {}", txt);
		    	   return null;
		       }
		} catch (Exception e) {
			log.error(e.getMessage());
			return txt;
		}
	}
}
