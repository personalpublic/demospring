# demoSpring
Microservicio diseñado obtención de datos de Yahoo Financing y para obtener la información de Dweet.io
La información obtenida se almacena en una base de datos MongoDB

## http://localhost:5050/joshe/yahooInsert
### Método de la solicitud:  *Get*
Obtiene la información de la api de Yahoo Financing, específicamente 5 días de información, dicha información se almacena en MongoDB en la entidad *YahooValue* 

## http://localhost:5050/joshe/yahooSelect
### Método de la solicitud:  *Post*
Permite consultar la información depositada en la base de datos, para lo cual se debe indicar el rango de tiempo a consultar.
### Request:
<div>
<div>{</div>
<div>&nbsp;&nbsp;&nbsp;&nbsp;"<strong>desde</strong>":"04/09/2020&nbsp;01:01:01",</div>
<div>&nbsp;&nbsp;&nbsp;&nbsp;"<strong>hasta</strong>":"05/09/2020&nbsp;01:01:01"</div>
<div>}</div>
</div>

## http://localhost:5050/joshe/yahooTime
### Método de la solicitud:  *Get*
Proceso creado para obtener información en un lapso de 15 minutos,  con un delay de 1 minuto

## IDE
Java 8
sts-4.7.2.RELEASE